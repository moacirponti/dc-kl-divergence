#ifndef _CLASSIFIERS_H_
#define _CLASSIFIERS_H_ 

#include "common.h"

// Classifies an Instance and returns its probability vector
ProbabilityVector* knnClassification(Dataset *S, Instance T, int k);

// Compute Euclidean distance between feature vectors
double knn_EuclDist(double *f1, double *f2, int n);

#endif