#ifndef _DCKLD_H_
#define _DCKLD_H_ 

#include "common.h"

/* Calculates Kullback-Leibler Divergence for two classifiers, 'P' and 'Q'
 *	input: two probability distribution vectors
 *	output: value of the KL divergence */
double KullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q);

/* Calculates Decision Cognizant Kullback-Leibler Divergence for two classifiers, 'P' and 'Q'
 *	input: two probability distribution vectors of size 3:
 *		-> [0] is the probability of class 'w', dominant of 'P'
 *		-> [1] is the probability of class 'w-til', dominant of 'Q'
 *		-> [2] is the probability of all other classes
 *	output: value of the KL divergence */
double DCKullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q);

#endif