#include "divergences.h"

int isDoubleEqual(double a, double b){
	return (fabs(a-b) < DOUBLE_SIMILARITY_THRESHOLD);
}

int main(int argc, char const *argv[])
{
	int i;
	FILE* outFile = fopen("out.txt", "w");

	srand(time(NULL)); // Starts random seed

	printf("Program that simulates an experiment for calculating regular\n");
	printf("  and decision cognizant KL divergences, given a fixed DC divergence value\n");
	printf("\n");
	printf("\tAuthors: \n");
	printf("\t  Mateus Riva (riva.mateus@gmail.com)\n");
	printf("\t  Moacir Ponti (moacirponti@gmail.com)\n");
	printf("\t  Josef Kittler (j.kittler@surrey.ac.uk)\n");
	printf("-------------------------------------------------------------\n\n");

	if (argc < 3){
		printf("Usage: regular_vs_dc <file> <value>\n");
		printf("\t<file>: probability vector pair file\n");
		printf("\t<value>: fixed DC K-L divergence value for sampling [put a negative value to output all]\n");
		return -1;
	}

	// Opening file from arguments
	FILE* infile = fopen(argv[1], "rb");
	double samplingValue = atof(argv[2]);

	// Getting number of classes and of experiments
	int k, n;
	fread(&k, sizeof(int), 1, infile);
	fread(&n, sizeof(int), 1, infile);

	// Checking if 'k' is valid
	if (k <= 1){
		fprintf(stderr, "Invalid number of classes! Exiting...\n");
		return -1;
	}

	// Running experiments
	for (i = 0; i < n; i++){
		ProbabilityVector* P = readProbabilityVector(infile);
		ProbabilityVector* Q = readProbabilityVector(infile);

		double KLD = KullbackLeiblerDivergence(P, Q);
		double DCKLD = DCKullbackLeiblerDivergence(P, Q);

		if (isDoubleEqual(samplingValue, DCKLD) || samplingValue <= 0) fprintf(outFile, "%lf\t%lf\n", KLD, DCKLD);

		destroyProbabilityVector(P);
		destroyProbabilityVector(Q);
	}

	fclose(outFile);

	return 0;
}