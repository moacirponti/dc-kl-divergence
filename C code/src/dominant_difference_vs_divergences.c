#include "divergences.h"

int main(int argc, char const *argv[])
{
	int i;
	FILE* outFile = fopen("out.txt", "w");

	srand(time(NULL)); // Starts random seed

	printf("Program that simulates an experiment for calculating dominant probability difference\n");
	printf("  against regular and decision-cognizant KL divergens\n");
	printf("\n");
	printf("\tAuthors: \n");
	printf("\t  Mateus Riva (riva.mateus@gmail.com)\n");
	printf("\t  Moacir Ponti (moacirponti@gmail.com)\n");
	printf("\t  Josef Kittler (j.kittler@surrey.ac.uk)\n");
	printf("-------------------------------------------------------------\n\n");

	if (argc < 2){
		printf("Usage: dominant_difference_vs_divergences <file>\n");
		printf("\t<file>: probability vector pair file\n");
		return -1;
	}

	// Opening file from arguments
	FILE* infile = fopen(argv[1], "rb");

	// Getting number of classes and of experiments
	int k, n;
	fread(&k, sizeof(int), 1, infile);
	fread(&n, sizeof(int), 1, infile);

	// Checking if 'k' is valid
	if (k <= 1){
		fprintf(stderr, "Invalid number of classes! Exiting...\n");
		return -1;
	}

	// Running experiments
	for (i = 0; i < n; i++){
		ProbabilityVector* P = readProbabilityVector(infile);
		ProbabilityVector* Q = readProbabilityVector(infile);

		double difference = DominantDifference(P, Q);

		double KLD = KullbackLeiblerDivergence(P, Q);
		double DCKLD = DCKullbackLeiblerDivergence(P, Q);

		fprintf(outFile, "%lf\t%lf\t%lf\n",difference, KLD, DCKLD);

		destroyProbabilityVector(P);
		destroyProbabilityVector(Q);
	}

	fclose(outFile);

	return 0;
}