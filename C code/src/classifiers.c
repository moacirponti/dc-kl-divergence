#include "classifiers.h"

// Classifies an Instance and returns its probability vector
ProbabilityVector* knnClassification(Dataset *S, Instance T, int k){
	int i, j, l;		// Iterators

	// Probability vector (which contains the answers)
	ProbabilityVector* classProbabilities = createProbabilityVector(S->nLabels);

	double* distancesToNN = (double*)malloc(sizeof(double)*k); // Ordered list of the distances to the k-NN
	int* kNN = (int*)malloc(sizeof(int)*k); // Ordered list of the k-NN
	for (i = 0; i < k; i++) distancesToNN[i] = DBL_MAX; // Initializing list with infinity
	for (i = 0; i < k; i++) kNN[i] = NIL; // Initializing list with NIL

	// Finds the k nearest neighbours in S, comparing each instance ('i')
	for (i = 0; i < S->nInstances; i++){
		// TODO: distinct distance functions
		double distance = knn_EuclDist(T.features,S->instances[i].features,S->nFeats);
		// tries to place it in the ordered list
		for (j = 0; j < k; j++){
			if (distance > distancesToNN[j]) break; // Further than this neighbour: end
			else if (distance < distancesToNN[j]) { // Closer than this neighbour
				if (j == k-1){ // Closer than the closest neighbour: shift and end
					for (l = 0; l < j-1; l++) {distancesToNN[l] = distancesToNN[l+1]; kNN[l] = kNN[l+1];}
					distancesToNN[j] = distance; kNN[j] = i;
					break;
				}
				 else if (distance >= distancesToNN[j+1]){ // Not closer than the next neighbour: shift and end
					for (l = 0; l < j-1; l++) {distancesToNN[l] = distancesToNN[l+1]; kNN[l] = kNN[l+1];}
					distancesToNN[j] = distance; kNN[j] = i;
					break;
				} else continue; // Closer than the next neighbour: continue					
			}
			else continue; // Equal to this neighbour: continue
		}
	}

	// Computing probability vector
	for (i = 0; i < k; i++)
		classProbabilities->values[S->instances[i].trueLabel-1] += 1;
	for (i = 0; i < classProbabilities->size; i++)
		classProbabilities->values[i] = classProbabilities->values[i] / k;
	

	free(distancesToNN);
	free(kNN);
	

	return classProbabilities;
}

// Compute Euclidean distance between feature vectors
double knn_EuclDist(double *f1, double *f2, int n){
	int i;
	double dist=0.0f;

	for (i=0; i < n; i++){
		dist += (f1[i]-f2[i])*(f1[i]-f2[i]);
	}

	return(sqrt(dist));
}