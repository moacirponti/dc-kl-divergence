#include "divergences.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

double u_rand()   /* uniform distribution, (0..1] */
{
	return (rand()+1.0)/(RAND_MAX+1.0);
}
double g_rand()  /* normal distribution, mean 0, std dev 1 */
{
	return sqrt(-2*log(u_rand())) * cos(2*M_PI*u_rand());
}

// Function that adds noise to a probability vector
ProbabilityVector* noiseProbabilityVector(ProbabilityVector* base, double sigma){
	int i;

	// Initializing prob. vector
	ProbabilityVector* v = createProbabilityVector(base->size);

	// Adding noise and normalizing
	double sum = 0.0;
	for (i = 0; i < v->size; i++){
		v->values[i] += g_rand()*sigma;
		if (v->values[i] < 0.0) v->values[i] = 0.0; // truncate negative values
		sum += v->values[i];
	}
	for (i = 0; i < v->size; i++)
		v->values[i] = v->values[i]/sum;

	return v;
}

int main(int argc, char const *argv[])
{
	int i;

	srand(time(NULL)); // Starts random seed

	printf("Program that generates noisy probability pairs from previously created probability pairs\n");
	printf("\n");
	printf("\tAuthors: \n");
	printf("\t  Mateus Riva (riva.mateus@gmail.com)\n");
	printf("\t  Moacir Ponti (moacirponti@gmail.com)\n");
	printf("\t  Josef Kittler (j.kittler@surrey.ac.uk)\n");
	printf("-------------------------------------------------------------\n\n");

	if (argc < 5){
		printf("Usage: create_noisy_vectors <file> <n> <s> <f>\n");
		printf("\t<file>: probability vector file to add noise to\n");
		printf("\t<n>: total number of vector pairs\n");
		printf("\t<s>: standard deviation of the noise\n");
		printf("\t<f>: output file name\n");
		return -1;
	}

	// Getting arguments
	int noise_n = atoi(argv[2]);
	double sigma = atof(argv[3]);
	FILE* outFile = fopen(argv[4], "wb");

	// Opening file from arguments
	FILE* infile = fopen(argv[1], "rb");

	// Getting number of classes and of experiments
	int k, n;
	fread(&k, sizeof(int), 1, infile);
	fread(&n, sizeof(int), 1, infile);

	// Checking if 'k' is valid
	if (k <= 1){
		fprintf(stderr, "Invalid number of classes! Exiting...\n");
		return -1;
	}

	// Running noisery
	int noise_n_total = noise_n*n;
	fwrite(&k, sizeof(int),1,outFile);
	fwrite(&noise_n_total, sizeof(int),1,outFile);
	/*fprintf(outFile, "%d %d\n", k, n);*/
	for (i = 0; i < n; i++){
		ProbabilityVector* P = readProbabilityVector(infile);
		ProbabilityVector* Q = readProbabilityVector(infile);

		int j;
		for (j = 0; j < noise_n; j++){
			ProbabilityVector* P_noisy = noiseProbabilityVector(P, sigma);
			ProbabilityVector* Q_noisy = noiseProbabilityVector(Q, sigma);
			writeProbabilityVector(P_noisy, outFile);
			writeProbabilityVector(Q_noisy, outFile);
			destroyProbabilityVector(P_noisy);
			destroyProbabilityVector(Q_noisy);
		}

		destroyProbabilityVector(P);
		destroyProbabilityVector(Q);
	}

	fclose(outFile);

	return 0;
}