#include "dataset.h"

// Creates a dataset with the specified header
Dataset* createDataset(int nInstances, int nLabels, int nFeats){
	Dataset* data = (Dataset*)malloc(sizeof(Dataset));

	data->nInstances = nInstances;
	data->nLabels = nLabels;
	data->nFeats = nFeats;

	data->instances = (Instance*)malloc(data->nInstances*sizeof(Instance));

	int i;
	for (i = 0; i < data->nInstances; i++) 
		data->instances[i].features = (double*)malloc(data->nFeats*sizeof(double));

	return data;
}

// Destroys a dataset
void destroyDataset(Dataset* data){
	if (data != NULL){
		if (data->instances != NULL){
			int i;
			for (i = 0; i < data->nInstances; i++) 
				if (data->instances[i].features != NULL) free(data->instances[i].features);
			free(data->instances);
		}
		free(data);
	}
}

// Reads a dataset from file in .csv format
Dataset* readDataset(const char* filename){
	// Opening file
	FILE* infile = fopen(filename, "r");

	// Reading header
	int nInstances, nLabels, nFeats;
	fscanf(infile,"%d,%d,%d", &nInstances, &nLabels, &nFeats);

	Dataset* data = createDataset(nInstances, nLabels, nFeats);

	int i, j;
	for (i = 0; i < data->nInstances; i++){
		for(j = 0; j < data->nFeats; j++)
			fscanf(infile,"%lf,",&(data->instances[i].features[j]));
		fscanf(infile,"%d",&(data->instances[i].trueLabel));
		data->instances[i].label = -1;
	}

	fclose(infile);

	return data;
}