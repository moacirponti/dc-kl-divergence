#include "common.h"

// Function that returns a random double inside a range
double randomRange(double a, double b) {
	double random = ((double) rand()) / (double) RAND_MAX;
	double diff = b - a;
	double r = random * diff;
	return a + r;
}

// Allocates a probability vector of specified size 
ProbabilityVector* createProbabilityVector(int size){
	ProbabilityVector* v = (ProbabilityVector*) malloc(sizeof(ProbabilityVector));

	v->values = (double*)calloc(size,sizeof(double));
	v->size = size;

	return v;
}

// Destroys a probability vector
void destroyProbabilityVector(ProbabilityVector* v){
	if (v != NULL){
		if (v->values != NULL) free(v->values);
		free(v);
	}
}

// Copies a probability vector into a new one
ProbabilityVector* copyProbabilityVector(ProbabilityVector* v){
	ProbabilityVector* newV = createProbabilityVector(v->size);
	int i;
	for (i = 0; i < newV->size; i++) newV->values[i] = v->values[i];
	return newV;
}

// Writes a probability vector to file
void writeProbabilityVector(ProbabilityVector* v, FILE* outfile){

	fwrite(&(v->size), sizeof(int), 1, outfile);
	fwrite(v->values, sizeof(double), v->size, outfile);

}

// Reads a probability vector from a file
ProbabilityVector* readProbabilityVector(FILE* infile){
	int size;
	fread(&(size), sizeof(int), 1, infile);
	ProbabilityVector* v = createProbabilityVector(size);
	fread(v->values, sizeof(double), v->size, infile);
	return v;
}

// Outputs a probability vector to a readable stream
void printProbabilityVector(ProbabilityVector* v, FILE* stream){
	int i;
	fprintf(stream, "%d [", v->size);
	for (i = 0; i < v->size-1; i++) fprintf(stream, "%.8lf, ", v->values[i]);
	fprintf(stream, "%.8lf]\n", v->values[i]);
}
// Reads a probability vector from a readable stream
ProbabilityVector* scanProbabilityVector(FILE* stream){
	ProbabilityVector* v; int size;
	int i;
	fscanf(stream, "%d [", &(size));
	v = createProbabilityVector(size);
	for (i = 0; i < v->size-1; i++) fscanf(stream, "%lf, ", &(v->values[i]));
	fscanf(stream, "%lf]\n", &(v->values[i]));
	return v; 
}

/* Groups non-dominant classes into a single probability
   After this, vectors will be formatted as such:
 *		-> [0] is the probability of class 'w', dominant of 'P'
 *		-> [1] is the probability of class 'w-til', dominant of 'Q'
 *		-> [2] is the probability of all other classes*/
/*void groupClutter(ProbabilityVector** P, ProbabilityVector** Q){
	if ((*P)->size != (*Q)->size){
		fprintf(stderr, "Error at groupClutter: Probability vectors are of different sizes! (%d and %d)\n", (*P)->size, (*Q)->size);
		return ;
	}

	int dominantInP = NIL, dominantInQ = NIL; // Dominant labels for each classifier
	double clutterValue = 0.0;	// Auxiliaty variable to store clutter probabilities
	double dominantPValue = 0.0; // Auxiliary variable for finding & storing dominant labels
	double dominantQValue = 0.0; // Auxiliary variable for finding & storing dominant labels

	int i;
	for (i = 0; i < (*P)->size; i++)
		if ((*P)->values[i] > dominantPValue){
			dominantPValue = (*P)->values[i]; dominantInP = i;
		}	
	for (i = 0; i < (*Q)->size; i++)
		if ((*Q)->values[i] > dominantQValue){
			dominantQValue = (*Q)->values[i]; dominantInQ = i;
		}
	
	// Rebuilding P
	dominantPValue = (*P)->values[dominantInP];
	dominantQValue = (*P)->values[dominantInQ];
	clutterValue = 0.0;
	for (i = 0; i < (*P)->size; i++) if (i != dominantInP && i != dominantInQ) clutterValue += (*P)->values[i];

	destroyProbabilityVector(*P);
	*P = createProbabilityVector(3);
	(*P)->values[0] = dominantPValue;
	(*P)->values[1] = dominantQValue;
	(*P)->values[2] = clutterValue;

	// Rebuilding Q
	dominantPValue = (*Q)->values[dominantInP];
	dominantQValue = (*Q)->values[dominantInQ];
	clutterValue = 0.0;
	for (i = 0; i < (*Q)->size; i++) if (i != dominantInP && i != dominantInQ) clutterValue += (*Q)->values[i];

	destroyProbabilityVector(*Q);
	*Q = createProbabilityVector(3);
	(*Q)->values[0] = dominantPValue;
	(*Q)->values[1] = dominantQValue;
	(*Q)->values[2] = clutterValue;
}*/