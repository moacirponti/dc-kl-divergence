#include "divergences.h"

/* Calculates Kullback-Leibler Divergence of classifier 'P' from 'Q'
 *	input: two probability distribution vectors
 *	output: value of the KL divergence */
double EpsilonBlurredKullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q, double epsilon){
	if (P->size != Q->size){
		fprintf(stderr, "Error at KLD: Probability vectors are of different sizes! (%d and %d)\n", P->size, Q->size);
		return -1;
	}

	double divergence = 0.0;	// Divergence value
	

	// Iterates over all classes, computing and adding the divergence
	
	int i;
	for (i = 0; i < P->size; i++)	
		divergence += Q->values[i]*log(Q->values[i]/(P->values[i]+epsilon));

	return divergence;
}

/* Calculates Decision Cognizant Kullback-Leibler Divergence for two classifiers, 'P' and 'Q'
 *	input: two probability distribution vectors
 *	output: value of the KL divergence */
double EpsilonBlurredDCKullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q, double epsilon){
	if (P->size != Q->size){
		fprintf(stderr, "Error at DCKLD: Probability vectors are of different sizes! (%d and %d)\n", P->size, Q->size);
		return -1;
	}

	int dominantInP = NIL, dominantInQ = NIL; // Dominant labels for each classifier
	double clutterValueP = 0.0, clutterValueQ = 0.0;	// Clutter values
	double dominantPValue = 0.0; // Auxiliary variable for finding & storing dominant labels
	double dominantQValue = 0.0; // Auxiliary variable for finding & storing dominant labels

	int i;
	for (i = 0; i < P->size; i++)
		if (P->values[i] > dominantPValue){
			dominantPValue = P->values[i]; dominantInP = i;
		}	
	for (i = 0; i < Q->size; i++)
		if (Q->values[i] > dominantQValue){
			dominantQValue = Q->values[i]; dominantInQ = i;
		}
	
	for(i = 0; i < P->size; i++){
		if (i != dominantInQ && i != dominantInP) {
			clutterValueP += P->values[i];
			clutterValueQ += Q->values[i];
		}
	}

	double divergence = 0.0;	// Divergence value

	divergence += Q->values[dominantInP]*log(Q->values[dominantInP]/(P->values[dominantInP] + epsilon));
	if (dominantInQ != dominantInP) divergence += Q->values[dominantInQ]*log(Q->values[dominantInQ]/(P->values[dominantInQ] + epsilon));
	divergence += clutterValueQ*log(clutterValueQ/(clutterValueP+epsilon));

	return divergence;
}

int main(int argc, char const *argv[])
{
	int i;
	FILE* outFile = fopen("out.txt", "w");

	srand(time(NULL)); // Starts random seed

	printf("Program that simulates an experiment for measuring the effect of epsilon\n");
	printf("  in KL divergences close to zero\n");
	printf("\n");
	printf("\tAuthors: \n");
	printf("\t  Mateus Riva (riva.mateus@gmail.com)\n");
	printf("\t  Moacir Ponti (moacirponti@gmail.com)\n");
	printf("\t  Josef Kittler (j.kittler@surrey.ac.uk)\n");
	printf("-------------------------------------------------------------\n\n");

	if (argc < 3){
		printf("Usage: epsilon_effect <file> <epsilon>\n");
		printf("\t<file>: probability vector pair file\n");
		printf("\t<epsilon>: value of epsilon\n");
		return -1;
	}

	// Opening file from arguments
	FILE* infile = fopen(argv[1], "rb");
	double epsilon = atof(argv[2]);

	// Getting number of classes and of experiments
	int k, n;
	fread(&k, sizeof(int), 1, infile);
	fread(&n, sizeof(int), 1, infile);

	// Checking if 'k' is valid
	if (k <= 1){
		fprintf(stderr, "Invalid number of classes! Exiting...\n");
		return -1;
	}

	// Running experiments
	for (i = 0; i < n; i++){
		ProbabilityVector* P = readProbabilityVector(infile);
		ProbabilityVector* Q = readProbabilityVector(infile);

		double KLD = EpsilonBlurredKullbackLeiblerDivergence(P, Q, epsilon);
		double DCKLD = EpsilonBlurredDCKullbackLeiblerDivergence(P, Q, epsilon);

		if (DCKLD <= 0 || KLD <= 0) fprintf(outFile, "%lf\t%lf\n", KLD, DCKLD);

		destroyProbabilityVector(P);
		destroyProbabilityVector(Q);
	}

	fclose(outFile);

	return 0;
}