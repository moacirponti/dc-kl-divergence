#include "divergences.h"

/* Calculates the difference between a the dominant hypothesis of 
 * a classifier 'P' and the same hypothesis in a classifier 'Q'
 *	input: two probability distribution vectors
 *	output: value of the difference */
double DominantDifference(ProbabilityVector* P, ProbabilityVector* Q){	
	if (P->size != Q->size){
		fprintf(stderr, "Error at DominantDifference: Probability vectors are of different sizes! (%d and %d)\n", P->size, Q->size);
		return -1;
	}

	int dominantInP = NIL; // Dominant labels for P classifier
	double dominantPValue = 0.0; // Auxiliary variable for finding & storing dominant labels

	int i;
	for (i = 0; i < P->size; i++)
		if (P->values[i] > dominantPValue){
			dominantPValue = P->values[i]; dominantInP = i;
		}	

	return fabs(P->values[dominantInP] - Q->values[dominantInP]);
}

/* Calculates Kullback-Leibler Divergence of classifier 'P' from 'Q'
 *	input: two probability distribution vectors
 *	output: value of the KL divergence */
double KullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q){
	if (P->size != Q->size){
		fprintf(stderr, "Error at KLD: Probability vectors are of different sizes! (%d and %d)\n", P->size, Q->size);
		return -1;
	}

	double divergence = 0.0;	// Divergence value
	

	// Iterates over all classes, computing and adding the divergence
	
	int i;
	for (i = 0; i < P->size; i++)	
		divergence += Q->values[i]*log(Q->values[i]/(P->values[i]+DBL_EPSILON));

	return divergence;
}

/* Calculates Decision Cognizant Kullback-Leibler Divergence for two classifiers, 'P' and 'Q'
 *	input: two probability distribution vectors
 *	output: value of the KL divergence */
double DCKullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q){
	if (P->size != Q->size){
		fprintf(stderr, "Error at DCKLD: Probability vectors are of different sizes! (%d and %d)\n", P->size, Q->size);
		return -1;
	}

	int dominantInP = NIL, dominantInQ = NIL; // Dominant labels for each classifier
	double clutterValueP = 0.0, clutterValueQ = 0.0;	// Clutter values
	double dominantPValue = 0.0; // Auxiliary variable for finding & storing dominant labels
	double dominantQValue = 0.0; // Auxiliary variable for finding & storing dominant labels

	int i;
	for (i = 0; i < P->size; i++)
		if (P->values[i] > dominantPValue){
			dominantPValue = P->values[i]; dominantInP = i;
		}	
	for (i = 0; i < Q->size; i++)
		if (Q->values[i] > dominantQValue){
			dominantQValue = Q->values[i]; dominantInQ = i;
		}
	
	for(i = 0; i < P->size; i++){
		if (i != dominantInQ && i != dominantInP) {
			clutterValueP += P->values[i];
			clutterValueQ += Q->values[i];
		}
	}

	double divergence = 0.0;	// Divergence value
	
	/*if (Q->values[dominantInP] != 0 && P->values[dominantInP] != 0) divergence += Q->values[dominantInP]*log(Q->values[dominantInP]/P->values[dominantInP]);
	if (dominantInQ != dominantInP && Q->values[dominantInQ] != 0 && P->values[dominantInQ] != 0) divergence += Q->values[dominantInQ]*log(Q->values[dominantInQ]/P->values[dominantInQ]);
	if (clutterValueQ != 0 && clutterValueP != 0) divergence += clutterValueQ*log(clutterValueQ/clutterValueP);
*/

	divergence += Q->values[dominantInP]*log(Q->values[dominantInP]/(P->values[dominantInP] + DBL_EPSILON));
	if (dominantInQ != dominantInP) divergence += Q->values[dominantInQ]*log(Q->values[dominantInQ]/(P->values[dominantInQ] + DBL_EPSILON));
	divergence += clutterValueQ*log(clutterValueQ/(clutterValueP+DBL_EPSILON));

	return divergence;
}

/* Calculates Kullback-Leibler Divergence for two classifiers' clutter, 'P' and 'Q'
 *	input: two probability distribution vectors
 *	output: value of the KL divergence */
double ClutterKullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q){
	if (P->size != Q->size){
		fprintf(stderr, "Error at ClutterKLD: Probability vectors are of different sizes! (%d and %d)\n", P->size, Q->size);
		return -1;
	}

	int dominantInP = NIL, dominantInQ = NIL; // Dominant labels for each classifier
	double dominantPValue = 0.0; // Auxiliary variable for finding & storing dominant labels
	double dominantQValue = 0.0; // Auxiliary variable for finding & storing dominant labels

	int i;
	for (i = 0; i < P->size; i++)
		if (P->values[i] > dominantPValue){
			dominantPValue = P->values[i]; dominantInP = i;
		}	
	for (i = 0; i < Q->size; i++)
		if (Q->values[i] > dominantQValue){
			dominantQValue = Q->values[i]; dominantInQ = i;
		}

	double divergence = 0.0; // Divergence value

	// Iterates over all clutter classes, computing and adding the divergence
	for (i = 0; i < P->size; i++)	
		if (i != dominantInP && i != dominantInQ)
			divergence += Q->values[i]*log(Q->values[i]/(P->values[i]+DBL_EPSILON));

	return divergence;

}

/* Calculates Decision Cognizant Kullback-Leibler Divergence for two classifiers' clutter, 'P' and 'Q'
 *	input: two probability distribution vectors of size 3:
 *		-> [0] is the probability of class 'w', dominant of 'P'
 *		-> [1] is the probability of class 'w-til', dominant of 'Q'
 *		-> [2] is the probability of all other classes
 *	output: value of the KL divergence */
double ClutterDCKullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q){
	if (P->size != Q->size){
		fprintf(stderr, "Error at ClutterKLD: Probability vectors are of different sizes! (%d and %d)\n", P->size, Q->size);
		return -1;
	}

	int dominantInP = NIL, dominantInQ = NIL; // Dominant labels for each classifier
	double clutterValueP = 0.0, clutterValueQ = 0.0;	// Clutter values
	double dominantPValue = 0.0; // Auxiliary variable for finding & storing dominant labels
	double dominantQValue = 0.0; // Auxiliary variable for finding & storing dominant labels

	int i;
	for (i = 0; i < P->size; i++)
		if (P->values[i] > dominantPValue){
			dominantPValue = P->values[i]; dominantInP = i;
		}	
	for (i = 0; i < Q->size; i++)
		if (Q->values[i] > dominantQValue){
			dominantQValue = Q->values[i]; dominantInQ = i;
		}
	
	for(i = 0; i < P->size; i++){
		if (i != dominantInQ && i != dominantInP) {
			clutterValueP += P->values[i];
			clutterValueQ += Q->values[i];
		}
	}

	double divergence = 0.0;	// Divergence value
	
	divergence += clutterValueQ*log(clutterValueQ/(clutterValueP+DBL_EPSILON));

	return divergence;
}