#include "divergences.h"

// Function that randomly draws a probability vector, possibly with a fixed dominant class
ProbabilityVector* drawProbabilityVector(int dominantClass, int totalOfClasses){
	int i;

	// Initializing prob. vector
	ProbabilityVector* v = createProbabilityVector(totalOfClasses);

	// Randomizing and normalizing all probabilities
	double sum = 0.0;
	while (sum <= 0.0){
		for (i = 0; i < totalOfClasses; i++){
			while(v->values[i] == 0) v->values[i] = ((double) rand()) / (double) RAND_MAX;	
			sum += v->values[i];
		}
		for (i = 0; i < totalOfClasses; i++)
			v->values[i] = v->values[i] / sum;
	}
	
	// If a dominant class is provided, then swap the greater probability to it
	if (dominantClass >= 0){
		double dominantValue = v->values[dominantClass]; int temporaryDominantClass = dominantClass;
		for (i = 0; i < totalOfClasses; i++)
			if (v->values[i] > dominantValue) {dominantValue = v->values[i]; temporaryDominantClass = i;}
		if (temporaryDominantClass != dominantClass){
			double swapAux = v->values[temporaryDominantClass];
			v->values[temporaryDominantClass] = v->values[dominantClass];
			v->values[dominantClass] = swapAux;
		}
	}

	return v;
}

int main(int argc, char const *argv[])
{
	int i;

	srand(time(NULL)); // Starts random seed

	printf("Program that generates random probability pairs\n");
	printf("\n");
	printf("\tAuthors: \n");
	printf("\t  Mateus Riva (riva.mateus@gmail.com)\n");
	printf("\t  Moacir Ponti (moacirponti@gmail.com)\n");
	printf("\t  Josef Kittler (j.kittler@surrey.ac.uk)\n");
	printf("-------------------------------------------------------------\n\n");

	if (argc < 4){
		printf("Usage: create_prob_vectors <k> <n> <f>\n");
		printf("\t<k>: total number of classes\n");
		printf("\t<n>: total number of vector pairs\n");
		printf("\t<f>: output file name\n");
		return -1;
	}

	// Getting arguments
	int k = atoi(argv[1]);
	int n = atoi(argv[2]);
	FILE* outFile = fopen(argv[3], "wb");

	// Checking if 'k' is valid
	if (k <= 1){
		fprintf(stderr, "Invalid number of classes! Exiting...\n");
		return -1;
	}

	// Running lottery
	fwrite(&k, sizeof(int),1,outFile);
	fwrite(&n, sizeof(int),1,outFile);
	/*fprintf(outFile, "%d %d\n", k, n);*/
	for (i = 0; i < n; i++){
		ProbabilityVector* P = drawProbabilityVector(-1, k);
		ProbabilityVector* Q = drawProbabilityVector(-1, k);

		/*printProbabilityVector(P, outFile);
		printProbabilityVector(Q, outFile);*/
		writeProbabilityVector(P, outFile);
		writeProbabilityVector(Q, outFile);

		destroyProbabilityVector(P);
		destroyProbabilityVector(Q);
	}

	fclose(outFile);

	return 0;
}