echo "Plotting file $1 on image $2..."
gnuplot <<- EOF
        set ylabel "K-L divergence"
        set xlabel "DC K-L divergence"
        set term png
        set title "$1" 
        set output "$2"
        plot "$1" using 2:1  every ::1 pt 2 lt 1 title ""
EOF