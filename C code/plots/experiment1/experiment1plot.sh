echo "Plotting file $1 on image $2..."
gnuplot <<- EOF
        set xlabel "Dominant Difference"
        set ylabel "K-L divergence"
        set term png
        set title "$1" 
        set output "KL$2"
        plot "$1" using 1:2  every ::1 pt 2 lt 1 title ""
EOF

gnuplot <<- EOF
        set xlabel "Dominant Difference"
        set ylabel "DC K-L divergence"
        set term png
        set title "$1" 
        set output "DC$2"
        plot "$1" using 1:3  every ::1 pt 2 lt 1 title ""
EOF
