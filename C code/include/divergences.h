#ifndef _DCKLD_H_
#define _DCKLD_H_ 

#include "common.h"

/* Calculates the difference between a the dominant hypothesis of 
 * a classifier 'P' and the same hypothesis in a classifier 'Q'
 *	input: two probability distribution vectors
 *	output: value of the difference */
double DominantDifference(ProbabilityVector* P, ProbabilityVector* Q);

/* Calculates Kullback-Leibler Divergence for two classifiers, 'P' and 'Q'
 *	input: two probability distribution vectors
 *	output: value of the KL divergence */
double KullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q);

/* Calculates Decision Cognizant Kullback-Leibler Divergence for two classifiers, 'P' and 'Q'
 *	input: two probability distribution vectors
 *	output: value of the KL divergence */
double DCKullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q);

/* Calculates Kullback-Leibler Divergence for two classifiers' clutter, 'P' and 'Q'
 *	input: two probability distribution vectors
 *	output: value of the KL divergence */
double ClutterKullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q);

/* Calculates Decision Cognizant Kullback-Leibler Divergence for two classifiers' clutter, 'P' and 'Q'
 *	input: two probability distribution vectors
 *	output: value of the KL divergence */
double ClutterDCKullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q);

#endif