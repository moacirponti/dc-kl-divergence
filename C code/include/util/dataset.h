#ifndef _DATASET_H_
#define _DATASET_H_ 

#include "common.h"

// Defining the structure of a single instance
typedef struct{
	int label;			// Classified label
	int trueLabel;		// True label
	double* features;	// Feature vector
} Instance;

// Defining the structure of an whole dataset
typedef struct{
	int nInstances;		// Size of dataset (number of instances)
	int nLabels;		// Number of distinct labels/classes
	int nFeats;			// Number of features per instance
	Instance* instances;// Instance vector
} Dataset;

// Creates a dataset with the specified header
Dataset* createDataset(int nInstances, int nLabels, int nFeats);

// Destroys a dataset
void destroyDataset(Dataset* data);

// Reads a dataset from file in .csv format
Dataset* readDataset(const char* filename);

#endif