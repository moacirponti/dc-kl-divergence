#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <limits.h>
#include <float.h>
#include <time.h>

#include "dataset.h"

// Defining threshold for double values similarity
#define DOUBLE_SIMILARITY_THRESHOLD 0.01

// Defining null value for integers
#define NIL -1

// Defining epsilon
#define EPSILON 0.0001

// Defining max filename size for fixed char buffers
#define MAX_FILENAME 256

// MIN operation
#ifndef MIN
#define MIN(x,y) (((x) < (y))?(x):(y))
#endif

// Function that returns a random double inside a range
double randomRange(double a, double b);

typedef struct{
	double* values;
	int size;
} ProbabilityVector;

// Allocates a probability vector of specified size 
ProbabilityVector* createProbabilityVector(int size);

// Destroys a probability vector
void destroyProbabilityVector(ProbabilityVector* v);

// Copies a probability vector into a new one
ProbabilityVector* copyProbabilityVector(ProbabilityVector* v);

// Writes a probability vector to file
void writeProbabilityVector(ProbabilityVector* v, FILE* outfile);

// Reads a probability vector from a file
ProbabilityVector* readProbabilityVector(FILE* infile);

// Outputs a probability vector to a readable stream
void printProbabilityVector(ProbabilityVector* v, FILE* stream);

// Reads a probability vector from a readable stream
ProbabilityVector* scanProbabilityVector(FILE* stream);

/* Groups non-dominant classes into a single probability
   After this, vectors will be formatted as such:
 *		-> [0] is the probability of class 'w', dominant of 'P'
 *		-> [1] is the probability of class 'w-til', dominant of 'Q'
 *		-> [2] is the probability of all other classes*/
//void groupClutter(ProbabilityVector** P, ProbabilityVector** Q);

#endif
