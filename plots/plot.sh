gnuplot <<- EOF
        set xlabel "K-L divergence"
        set ylabel "DC K-L divergence"
        set term png
        set title "" 
        set output "$2"
        plot "$1" using 1:2  every ::1 pt 2 lt 1 title ""
EOF
