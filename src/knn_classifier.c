#include "classifiers.h"

int main(int argc, char const *argv[])
{
	printf("Program that uses a kNN classifier to classify a dataset against another, and outputs the prob. vectors \n");
	printf("\n");
	printf("\tAuthors: Mateus Riva (riva.mateus@gmail.com), Moacir Ponti (moacirponti@gmail.com), Josef Kittler (j.kittler@surrey.ac.uk)\n");
	printf("---------------------------------------------------------------------\n\n");

	if (argc < 4){
		printf("Usage: knn_classifier <S> <T> <k>\n");
		printf("\t<S>: filename of the training dataset\n");
		printf("\t<T>: filename of the testing dataset\n");
		printf("\t<k>: number of neighbours to use in the classification\n");
		return -1;
	}

	// Reading datasets from the arguments
	Dataset* S = readDataset(argv[1]);
	Dataset* T = readDataset(argv[2]);

	// Reading 'k' from the arguments
	int k = atoi(argv[3]);

	// Classifying each instance of T and outputting to a file
	int i;
	for (i = 0; i < T->nInstances; i++){
		ProbabilityVector* v = knnClassification(S, T->instances[i], k);

		char filename[MAX_FILENAME];
		sprintf(filename, "vector-%dnn-%s-%d",k, argv[2], i);

		writeProbabilityVector(v, filename);

		printf("%s: ", filename);
		int j = 0;
		for (j = 0; j < v->size; j++) {printf("%lf, ", v->values[j]);
		printf("\n");}
	}

	return 0;
}