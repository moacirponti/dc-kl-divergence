#include "divergences.h"

#define NUMBER_OF_EXPERIMENTS 1000000 // Total number of experiments to be drawn

// Function that randomly draws a probability vector, possibly with a fixed dominant class
ProbabilityVector* drawProbabilityVector(int dominantClass, int totalOfClasses){
	int i;

	// Initializing prob. vector
	ProbabilityVector* v = createProbabilityVector(totalOfClasses);

	// Randomizing and normalizing all probabilities
	double sum = 0.0;
	while (sum <= 0.0){
		for (i = 0; i < totalOfClasses; i++){
			while(v->values[i] == 0) v->values[i] = ((double) rand()) / (double) RAND_MAX;	
			sum += v->values[i];
		}
		for (i = 0; i < totalOfClasses; i++)
			v->values[i] = v->values[i] / sum;
	}
	
	// If a dominant class is provided, then swap the greater probability to it
	if (dominantClass >= 0){
		double dominantValue = v->values[dominantClass]; int temporaryDominantClass = dominantClass;
		for (i = 0; i < totalOfClasses; i++)
			if (v->values[i] > dominantValue) {dominantValue = v->values[i]; temporaryDominantClass = i;}
		if (temporaryDominantClass != dominantClass){
			double swapAux = v->values[temporaryDominantClass];
			v->values[temporaryDominantClass] = v->values[dominantClass];
			v->values[dominantClass] = swapAux;
		}
	}

	return v;
}

int main(int argc, char const *argv[])
{
	int i;
	FILE* outFile = fopen("out.txt", "w");

	srand(time(NULL)); // Starts random seed

	printf("Program that simulates an experiment for calculating regular\n");
	printf("  and decision cognizant KL divergences\n");
	printf("\n");
	printf("\tAuthors: \n");
	printf("\t  Mateus Riva (riva.mateus@gmail.com)\n");
	printf("\t  Moacir Ponti (moacirponti@gmail.com)\n");
	printf("\t  Josef Kittler (j.kittler@surrey.ac.uk)\n");
	printf("-------------------------------------------------------------\n\n");

	if (argc < 2){
		printf("Usage: simulate_experiment <k>\n");
		printf("\t<k>: total number of classes for experiment\n");
		return -1;
	}

	// Getting number of classes from arguments
	int k = atoi(argv[1]);

	// Checking if 'k' is valid
	if (k <= 1){
		fprintf(stderr, "Invalid number of classes! Exiting...\n");
		return -1;
	}

	// Running experiments
	for (i = 0; i < NUMBER_OF_EXPERIMENTS; i++){
		// We'll consider the dominant class 'u' of the prob. vector 'P' to be 0
		ProbabilityVector* P = drawProbabilityVector(0, k);
		ProbabilityVector* Q = drawProbabilityVector(-1, k);

		/*
		ProbabilityVector* Paux = copyProbabilityVector(P);
		ProbabilityVector* Qaux = copyProbabilityVector(Q);
		*/

		double KLD = KullbackLeiblerDivergence(P, Q);
		groupClutter(&P,&Q);
		double DCKLD = DCKullbackLeiblerDivergence(P, Q);

		// Report on vectors whose DCKLD is greater than their KLD (the "tongue")
		/*
		if (DCKLD > KLD) {
			printf("DCKLD > KLD on:\n");
			printf("\t P: "); printProbabilityVector(Paux, stdout);
			printf("\t Q: "); printProbabilityVector(Qaux, stdout);
		}
		*/

		// Report on vectors whose DCKLD is zero (the "comet's other tail")
		/*
		if (DCKLD <= 0.001) {
			printf("DCKLD == 0 and KLD = %lf on:\n", KLD);
			printf("\t P: "); printProbabilityVector(Paux, stdout);
			printf("\t Q: "); printProbabilityVector(Qaux, stdout);
		}
		*/

		fprintf(outFile, "%lf\t%lf\n", KLD, DCKLD);

		destroyProbabilityVector(P);
		destroyProbabilityVector(Q);
	}

	fclose(outFile);

	return 0;
}