#include "divergences.h"

/* Calculates Kullback-Leibler Divergence of classifier 'P' from 'Q'
 *	input: two probability distribution vectors
 *	output: value of the KL divergence */
double KullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q){
	if (P->size != Q->size){
		fprintf(stderr, "Error at KLD: Probability vectors are of different sizes! (%d and %d)\n", P->size, Q->size);
		return -1;
	}

	double divergence = 0.0;	// Divergence value
	

	// Iterates over all classes, computing and adding the divergence
	int i;
	for (i = 0; i < P->size; i++)	
		if (Q->values[i] != 0 && P->values[i] != 0) divergence += P->values[i]*log(P->values[i]/Q->values[i]);

	return divergence;
}

/* Calculates Decision Cognizant Kullback-Leibler Divergence for two classifiers, 'P' and 'Q'
 *	input: two probability distribution vectors of size 3:
 *		-> [0] is the probability of class 'w', dominant of 'P'
 *		-> [1] is the probability of class 'w-til', dominant of 'Q'
 *		-> [2] is the probability of all other classes
 *	output: value of the KL divergence */
double DCKullbackLeiblerDivergence(ProbabilityVector* P, ProbabilityVector* Q){
	if (P->size != 3 || Q->size != 3){
		fprintf(stderr, "Error at DCKLD: Probability vectors are of wrong sizes! (%d and %d, should be 3)\n", P->size, Q->size);
		return -1;
	}

	double divergence = 0.0;	// Divergence value
	
	int i;

	// Iterates over all classes, computing and adding the divergence
	for (i = 0; i < 3; i++)
		if (Q->values[i] != 0 && P->values[i] != 0) divergence += P->values[i]*log(P->values[i]/Q->values[i]);
	
	return divergence;
}