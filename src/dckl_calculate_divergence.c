#include "divergences.h"

int main(int argc, char const *argv[])
{
	char filenameOut[MAX_FILENAME];
	int isDC = 0;	// Is Decision-Cognizant

	printf("Program that calculates regular and decision cognizant KL divergences\n");
	printf("\n");
	printf("\tAuthors: Mateus Riva (riva.mateus@gmail.com), Moacir Ponti (moacirponti@gmail.com), Josef Kittler (j.kittler@surrey.ac.uk)\n");
	printf("---------------------------------------------------------------------\n\n");

	if (argc < 3){
		printf("Usage: dckl_calculate_divergence <P> <Q> [<DC>]\n");
		printf("\t<P>: filename of the P-classifier probability vector\n");
		printf("\t<Q>: filename of the Q-classifier probability vector\n");
		printf("\t<DC>: type of KL-Divergence to use (0: regular [default], 1: decision cognizant)\n");
		return -1;
	}

	// Getting probability vectors from arguments
	ProbabilityVector* P = readProbabilityVector(argv[1]);
	ProbabilityVector* Q = readProbabilityVector(argv[2]);
	if (P == NULL || Q == NULL) return -1;

	// Getting isDC from arguments
	if (argc == 4) isDC = atoi(argv[3]);

	// Calculating divergence
	double divergence;
	if (isDC) {
		groupClutter(&P, &Q);
		divergence = DCKullbackLeiblerDivergence(P, Q);
	}
	else divergence = KullbackLeiblerDivergence(P, Q);

	printf("%s divergence is %lf\n", isDC ? "DC-KL " : "R-KL ", divergence);

	// Outputting divergence to file
	sprintf(filenameOut, "%s-%s-%s.out", isDC ? "DC" : "R", argv[1], argv[2]);
	FILE* outFile = fopen(filenameOut, "w");
	fprintf(outFile, "%lf\n", divergence);
	fclose(outFile);


	destroyProbabilityVector(P);
	destroyProbabilityVector(Q);

	return 0;
}