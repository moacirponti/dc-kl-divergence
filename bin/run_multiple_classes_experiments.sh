#!/bin/bash
echo "Running DCKL experiments..."

for j in 2 3 5 10 25 50 75 100
do
	echo "$j classes" >> log.txt
	{
		./simulate_experiment $j
		mv out.txt out$(printf "%02d" $j)classes.txt
	} &>> log.txt

done