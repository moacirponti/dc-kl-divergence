LIB=./lib
INCLUDE=./include
SRC=./src
OBJ=./obj
UTIL=util

CC=gcc

FLAGS=  -O3 -g -Wall


INCFLAGS = -I$(INCLUDE) -I$(INCLUDE)/$(UTIL)

all: libDCKL simulate_experiment #dckl_calculate_divergence knn_classifier

libDCKL: libDCKL-build
	echo "libDCKL.a built..."

libDCKL-build: \
util \
$(OBJ)/divergences.o \
$(OBJ)/classifiers.o \

	ar csr $(LIB)/libDCKL.a \
$(OBJ)/common.o \
$(OBJ)/dataset.o \
$(OBJ)/divergences.o \
$(OBJ)/classifiers.o \

$(OBJ)/divergences.o: $(SRC)/divergences.c
	$(CC) $(FLAGS) -g -c $(SRC)/divergences.c $(INCFLAGS) \
	-o $(OBJ)/divergences.o

$(OBJ)/classifiers.o: $(SRC)/classifiers.c
	$(CC) $(FLAGS) -g -c $(SRC)/classifiers.c $(INCFLAGS) \
	-o $(OBJ)/classifiers.o

dckl_calculate_divergence: libDCKL
	$(CC) $(FLAGS) $(INCFLAGS) src/dckl_calculate_divergence.c  -L./lib -o bin/dckl_calculate_divergence -lDCKL -lm

knn_classifier: libDCKL
	$(CC) $(FLAGS) $(INCFLAGS) src/knn_classifier.c  -L./lib -o bin/knn_classifier -lDCKL -lm

simulate_experiment: libDCKL
	$(CC) $(FLAGS) $(INCFLAGS) src/simulate_experiment.c  -L./lib -o bin/simulate_experiment -lDCKL -lm


util: $(SRC)/$(UTIL)/common.c $(SRC)/$(UTIL)/dataset.c
	$(CC) $(FLAGS) $(INCFLAGS) -c $(SRC)/$(UTIL)/common.c -o $(OBJ)/common.o
	$(CC) $(FLAGS) $(INCFLAGS) -c $(SRC)/$(UTIL)/dataset.c -o $(OBJ)/dataset.o


## Cleaning-up

clean:
	rm -f $(LIB)/lib*.a; rm -f $(OBJ)/*.o bin/simulate_experiment

